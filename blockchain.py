#!/usr/bin/python3
# -*- coding:utf-8 -*-
# __author__ = 'liao gao xiang'

import json
import hashlib
import requests
from time import time
from textwrap import dedent
from uuid import uuid4
from urllib.parse import urlparse

from flask import Flask, jsonify, request


class Blockchain(object):
    def __init__(self):
        self.chain = []  # 区块链
        self.nodes = set()  # 节点(集合的三大特性：确定性、互异性、唯一性)
        self.current_transactions = []  # 交易信息

        # 新建创世区块
        self.new_block(previous_hash=1, proof=100)

    def new_block(self, proof, previous_hash=None):
        """
        创建新的区块并添加到区块链

        :param proof: <int> PoW工作量证明
        :param previous_hash: <str> 上一个区块的Hash
        :return: 新的区块
        """
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transactions': self.current_transactions,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
        }

        # 重置当前区块中的交易
        self.current_transactions = []

        self.chain.append(block)
        return block

    def new_transaction(self, sender, recipient, amount):
        """
        添加新的交易到交易信息

        :param sender: <str> 发送者的地址
        :param recipient: <str> 接收者的地址
        :param amount: <str> 交易中的数量
        :return: <int> 包含该交易的区块索引
        """
        self.current_transactions.append({
            'sender': sender,
            'recipient': recipient,
            'amount': amount,
        })

        return self.last_block['index'] + 1

    @property
    def last_block(self):
        """返回区块链中的最后一个区块"""
        return self.chain[-1]

    @staticmethod
    def hash(block):
        """
        生成区块的哈希值

        :param block: <dict> 区块
        :return: <str> Hash值
        """

        # 确保字典有序，否则可能得到不一样的Hash值
        block_string = json.dumps(block, sort_keys=True).encode('utf-8')
        return hashlib.sha256(block_string).hexdigest()

    def proof_of_work(self, last_proof):
        """
        简单的工作量证明机制
        - 假设上一次工作量的值为prev, 寻找工作量curr, 使得hash(prev curr)的结果是否是0000开头

        :param last_proof: <int>
        :return: <int>
        """

        proof = 0
        while self.valid_proof(last_proof, proof) is False:
            proof += 1
        return proof

    @staticmethod
    def valid_proof(last_proof, proof):
        """
        工作量有效性证明：hash(last_proof, proof)结果是否是0000开头

        :param last_proof: <int> 上一个工作量
        :param proof: <int> 当前工作量
        :return: <bool> True证明有效, False证明错误
        """

        guess = f'{last_proof}{proof}'.encode('utf-8')
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == "0000"

    def register_node(self, address):
        """
        添加新节点到节点集

        :param address: <str> 节点的地址，如: 'http://192.168.1.1:80'
        :return: None
        """

        parsed_url = urlparse(address)
        self.nodes.add(parsed_url.netloc)

    def valid_chain(self, chain):
        """
        验证区块链是否合法

        :param chain: <list> 区块链
        :return: <bool> True表示合法，False表示非法
        """

        last_block = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]
            print(f"{last_block}")
            print(f"{block}")
            # 检验区块的hash值是否正确
            if block['previous_hash'] != self.hash(last_block):
                return False
            # 检验PoW值是否正确
            if not self.valid_proof(last_block['proof'], block['proof']):
                return False

            last_block = block
            current_index += 1

        return True

    def resolve_conflicts(self):
        """
        共识算法：将网络中的最长链作为合法的区块链

        :return: <bool> True表示区块链被替代，False表示没有被替代
        """

        neighbors = self.nodes
        new_chain = None

        # 只寻找比本节点更长的区块链
        max_length = len(self.chain)

        # 获取并验证网络中的区块链
        for node in neighbors:
            response = requests.get(f"http://{node}/chain")
            if 200 == response.status_code:
                length = response.json()['length']
                chain = response.json()['chain']
                # 验证区块链的长度以及合法性
                if length > max_length and self.valid_chain(chain):
                    max_length = length
                    new_chain = chain

        # 如何发现本节点更长的合法区块链则替换
        if new_chain:
            self.chain = new_chain
            return True

        return False


# 初始化节点
app = Flask(__name__)

# 为节点生成全局唯一地址
node_identifier = str(uuid4()).replace('-', '')

# 初始化区块链
blockchain = Blockchain()


@app.route('/mine', methods=['GET'])
def mine():
    # 通过PoW计算下一个工作量（即挖矿）
    last_block = blockchain.last_block
    last_proof = last_block['proof']
    proof = blockchain.proof_of_work(last_proof)

    # 挖矿成功则得到奖励，发送者为“0”表示这个节点挖出一个币
    blockchain.new_transaction(
        sender="0",
        recipient=node_identifier,
        amount=1
    )

    # 将新的区块添加到区块链中
    previous_hash = blockchain.hash(last_block)
    block = blockchain.new_block(proof, previous_hash)

    response = {
        'message': '新区块添加完成',
        'index': block['index'],
        'transactions': block['transactions'],
        'proof': block['proof'],
        'previous_hash': block['previous_hash'],
    }

    return jsonify(response), 200


@app.route('/transactions/new', methods=['POST'])
def new_transaction():
    values = request.get_json()

    # 校验POST数据的正确性
    required = ['sender', 'recipient', 'amount']
    if not all(k in values for k in required):
        return '请求参数错误！', 400

    # 生成新的交易
    index = blockchain.new_transaction(values['sender'], values['recipient'], values['amount'])
    response = {'message': f'交易将被添加到索引为{index}的区块中'}

    return jsonify(response), 201


@app.route('/chain', methods=['GET'])
def full_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain)
    }
    return jsonify(response), 200


@app.route('/nodes/register', methods=['POST'])
def register_node():
    values = request.get_json()
    nodes = values.get("nodes")

    if nodes is None:
        return "错误：请提供有效的节点列表", 400

    for node in nodes:
        blockchain.register_node(node)

    response = {
        'message': '节点添加成功',
        'total_nodes': list(blockchain.nodes)
    }

    return jsonify(response), 201


@app.route('/node/resolve', methods=['GET'])
def consensus():
    replaced = blockchain.resolve_conflicts()
    if replaced:
        response = {
            'message': '本节点的区块链被替代',
            'new_chain': blockchain.chain
        }
    else:
        response = {
            'message': '本节点的区块链为合法链',
            'chain': blockchain.chain
        }

    return jsonify(response), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=800)
